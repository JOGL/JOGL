# frozen_string_literal: true

after 'development:users' do
  space = Space.create!(description: FFaker::CheesyLingo.paragraph[0..511],
                        owner: User.first,
                        title: FFaker::CheesyLingo.title[0..127])

  (0..2).each do |n|
    space.text_sections << TextSection.new(category: :read_more,
                                           content: FFaker::CheesyLingo.paragraph[0..511],
                                           position: n,
                                           title: FFaker::CheesyLingo.title[0..127])
  end

  puts "SPACE      (id) #{space.id.colorize(:cyan)} (slug) #{space.slug.colorize(:cyan)}"
end
