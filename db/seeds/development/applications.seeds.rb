# frozen_string_literal: true

after 'development:projects', 'development:activities' do
  Application.create(peer_review: PeerReview.first,
                     project: Project.first)
end
