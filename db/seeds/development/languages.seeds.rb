# frozen_string_literal: true

after do
  Language.create!(code: 'DE', name: 'German', supported: true)
  Language.create!(code: 'EN', name: 'English', supported: true)
  Language.create!(code: 'FR', name: 'French', supported: true)
end
