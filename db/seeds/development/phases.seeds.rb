# frozen_string_literal: true

after 'development:users', 'development:projects' do
  Phase.create!(project: Project.first,
                start: Time.zone.today,
                stop: Time.zone.tomorrow,
                title: FFaker::CheesyLingo.title[0..127])
end
