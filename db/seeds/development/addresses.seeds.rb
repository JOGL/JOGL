# frozen_string_literal: true

after 'development:countries_divisions' do
  Address.create!(city_locality: FFaker::AddressFR.city,
                  country: Country.find('FR'),
                  division: Division.find_by(country_code: 'FR'),
                  postal_code: FFaker::AddressFR.postal_code,
                  street: FFaker::AddressFR.street_address)
end
