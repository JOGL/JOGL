# frozen_string_literal: true

after 'development:users' do
  project = Project.create!(description: FFaker::CheesyLingo.sentence[0..511],
                            owner: User.first,
                            space: Space.first,
                            title: FFaker::CheesyLingo.title[0..127])

  puts "PROJECT    (id) #{project.id.colorize(:cyan)} (slug) #{project.slug.colorize(:cyan)}"
end
