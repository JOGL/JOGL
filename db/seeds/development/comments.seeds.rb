# frozen_string_literal: true

after 'development:posts' do
  comment = Comment.new(content: FFaker::CheesyLingo.paragraph[0..255], owner: User.first)

  Post.first.comments << comment
end
