# frozen_string_literal: true

after 'development:users', 'development:spaces' do
  group = Group.create!(name: FFaker::Company.position, resource: Space.first)

  group.users << User.first
end
