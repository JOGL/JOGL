# frozen_string_literal: true

after 'development:languages' do
  SustainableDevelopmentGoal.find_or_create_by!({ number: 1, name: 'No poverty', color: 'EC1B30' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 2, name: 'Zero hunger', color: 'D49F2B' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 3, name: 'Good health and well-being', color: '2DA354' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 4, name: 'Quality eductation', color: 'CB233B' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 5, name: 'Gender equality', color: 'ED4129' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 6, name: 'Clean water and sanitation', color: '02B7DF' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 7, name: 'Affordable and clean energy', color: 'FEBF12' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 8, name: 'Decent work and economic growth', color: '981A41' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 9, name: 'Industry, innovation and infrastructure', color: 'F37634' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 10, name: 'Reduced inequalities', color: 'DE1768' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 11, name: 'Sustainable cities and communities', color: 'FA9D26' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 12, name: 'Responsible consumption and production', color: 'C69932' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 13, name: 'Climate action', color: '498A50' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 14, name: 'Life below water', color: '347DB7' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 15, name: 'Life and land', color: '40B04A' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 16, name: 'Peace, justice and strong institutions', color: '02558B' })
  SustainableDevelopmentGoal.find_or_create_by!({ number: 17, name: 'Partnerships for the goals', color: '1B366B' })
end
