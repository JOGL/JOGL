# frozen_string_literal: true

after 'development:users' do
  asset = Asset.new(name: FFaker::Product.product_name[0..63])

  Space.first.assets << asset

  User.first.assets << asset
end
