class CreateProposals < ActiveRecord::Migration[7.0]
  def change
    create_table :proposals, id: :uuid do |t|
      t.uuid :peer_review_id, null: false
      t.uuid :project_id, null: false

      t.integer :score
      t.string :title, null: false, limit: 128

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
