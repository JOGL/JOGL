# frozen_string_literal: true

class CreateNotifications < ActiveRecord::Migration[7.0]
  def change
    create_table :notifications, id: :uuid do |t|
      t.uuid :recipient_id, null: false
      t.string :recipient_type, null: false

      t.jsonb :params
      t.string :type, null: false
      t.datetime :read_at

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :notifications, :params, using: :gin
    add_index :notifications, :read_at, name: 'notifications_read_at_index'
  end
end
