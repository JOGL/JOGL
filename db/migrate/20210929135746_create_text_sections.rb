# frozen_string_literal: true

class CreateTextSections < ActiveRecord::Migration[7.0]
  def change
    create_table :text_sections, id: :uuid do |t|
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false

      t.integer :category, null: false, default: 0
      t.string :content, limit: 1024
      t.boolean :deleted, null: false, default: false
      t.integer :position, null: false, default: 0
      t.bigint :search_id
      t.string :title, null: false, limit: 128

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :text_sections, %i[resource_id resource_type], name: 'text_sections_resource_index'
  end
end
