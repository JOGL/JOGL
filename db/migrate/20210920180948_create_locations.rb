# frozen_string_literal: true

class CreateLocations < ActiveRecord::Migration[7.0]
  def change
    create_table :locations, id: :uuid do |t|
      t.uuid :address_id, foreign_key: true, index: true

      t.float :latitude
      t.float :longitude
      t.string :name, limit: 64, index: true
      t.bigint :search_id
    end
  end
end
