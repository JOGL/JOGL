# frozen_string_literal: true

class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects, id: :uuid do |t|
      t.uuid :location_id, foreign_key: true, index: true
      t.uuid :owner_id, null: false, foreign_key: { to_table: :users }, index: true
      t.uuid :space_id, foreign_key: true, index: true

      t.boolean :deleted, null: false, default: false, index: true
      t.string :description, null: false, limit: 1024
      t.bigint :search_id
      t.string :slug, null: false, index: { unique: true }, limit: 64
      t.string :summary, limit: 512
      t.string :title, null: false, limit: 128
      t.integer :visibility, null: false, default: 0, index: true

      t.integer :moments_count, null: false, default: 0
      t.integer :phases_count, null: false, default: 0

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
