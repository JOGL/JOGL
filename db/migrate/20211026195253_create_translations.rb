# frozen_string_literal: true

class CreateTranslations < ActiveRecord::Migration[7.0]
  def change
    create_table :translations, id: :uuid do |t|
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false

      t.string :content, null: false, length: 1024
      t.boolean :edited, null: false, default: false
      t.string :field, null: false
      t.string :language_code, null: false, foreign_key: { to_table: :languages, column: :code }, limit: 2
      t.bigint :search_id

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :translations, %i[resource_id resource_type]
    add_index :translations, %i[field resource_id resource_type], name: 'translation_field_index'
    add_index :translations, %i[language_code resource_id resource_type], name: 'translation_language_index'
    add_index :translations, %i[field language_code resource_id resource_type], unique: true, name: 'unique_translation_index'
  end
end
