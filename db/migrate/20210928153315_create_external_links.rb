# frozen_string_literal: true

class CreateExternalLinks < ActiveRecord::Migration[7.0]
  def change
    create_table :external_links, id: :uuid do |t|
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false

      t.string :text, null: false, limit: 128
      t.string :url, null: false, limit: 128

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
