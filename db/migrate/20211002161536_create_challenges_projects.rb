class CreateChallengesProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :challenges_projects, id: :uuid do |t|
      t.uuid :challenge_id, null: false, foreign_key: true, index: true
      t.uuid :project_id, null: false, foreign_key: true, index: true

      t.boolean :accepted, null: false, default: false
      
      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
