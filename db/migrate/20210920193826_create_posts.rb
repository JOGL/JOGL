# frozen_string_literal: true

class CreatePosts < ActiveRecord::Migration[7.0]
  def change
    create_table :posts, id: :uuid do |t|
      t.uuid :feed_id, null: false, foreign_key: true, index: true
      t.uuid :owner_id, null: false, foreign_key: { to_table: :users }, index: true

      t.integer :category, null: false, default: 0
      t.string :content, null: false, limit: 1024
      t.boolean :deleted, null: false, default: false
      t.bigint :search_id

      t.integer :comments_count, null: false, default: 0

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
