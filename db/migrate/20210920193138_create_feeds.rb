# frozen_string_literal: true

class CreateFeeds < ActiveRecord::Migration[7.0]
  def change
    create_table :feeds, id: :uuid do |t|
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false

      t.boolean :deleted, null: false, default: false
      t.jsonb :options, null: false, default: {}

      t.integer :posts_count, null: false, default: 0

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :feeds, :options, using: :gin
    add_index :feeds, %i[resource_id resource_type], unique: true, name: 'unique_feeds_resource_index'
  end
end
