# frozen_string_literal: true

class CreateGroups < ActiveRecord::Migration[7.0]
  def change
    create_table :groups, id: :uuid do |t|
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false

      t.string :name, index: true, limit: 64

      t.integer :users_count, null: false, default: 0

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    create_table :users_groups, id: false do |t|
      t.uuid :group_id, null: false, foreign_key: true, index: true
      t.uuid :user_id, null: false, foreign_key: true, index: true
    end

    add_index :groups, %i[resource_id resource_type], name: 'groups_resource_index'
    add_index :users_groups, %i[group_id user_id], unique: true, name: 'unique_user_group_index'
  end
end
