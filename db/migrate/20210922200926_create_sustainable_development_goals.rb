# frozen_string_literal: true

class CreateSustainableDevelopmentGoals < ActiveRecord::Migration[7.0]
  def change
    create_table :sustainable_development_goals, id: :uuid do |t|
      t.string :color, null: false, default: '000000', limit: 6
      t.string :name, null: false, index: { unique: true }, limit: 64
      t.integer :number, null: false, index: { unique: true }
      t.bigint :search_id
    end

    create_table :sustainable_development_goals_resources do |t|
      t.uuid :sustainable_development_goal_id, null: false, foreign_key: true

      t.uuid :resource_id, null: false
      t.string :resource_type, null: false
    end

    add_index :sustainable_development_goals_resources,
              :sustainable_development_goal_id,
              name: 'sustainable_development_goals_resources_index'
    add_index :sustainable_development_goals_resources,
              %i[resource_id resource_type],
              name: 'sustainable_development_goals_resource_index'
    add_index :sustainable_development_goals_resources,
              %i[sustainable_development_goal_id resource_id resource_type],
              unique: true,
              name: 'unique_sustainable_development_goals_resource_index'
  end
end
