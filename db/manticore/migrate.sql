CREATE TABLE assets (
  created_at timestamp,
  name text,
  resource_id string,
  resource_type string,
  updated_at timestamp
)
---
CREATE TABLE challenges (
  country_code string,
  created_at timestamp,
  deadline timestamp,
  description text,
  division_code string,
  location_latitude float,
  location_longitude float,
  resource_id string,
  resource_type string,
  start timestamp,
  stop timestamp,
  summary text,
  title text,
  updated_at timestamp,
  visibility string
)
---
CREATE TABLE comments (
  content text,
  created_at timestamp,
  owner_id string,
  post_id string,
  resource_id string,
  resource_type string,
  updated_at timestamp
)
---
CREATE TABLE countries (
  code text,
  name text,
  resource_id string,
  resource_type string
)
---
CREATE TABLE divisions (
  code text,
  country_code string,
  name text,
  resource_id string,
  resource_type string
)
---
CREATE TABLE events (
  country_code string,
  created_at timestamp,
  deadline timestamp,
  description text,
  division_code string,
  location_latitude float,
  location_longitude float,
  resource_id string,
  resource_type string,
  start timestamp,
  stop timestamp,
  updated_at timestamp,
  visibility string
)
---
CREATE TABLE languages (
  code text,
  name text,
  resource_id string,
  resource_type string
)
---
CREATE TABLE locations (
  country_code string,
  division_code string,
  latitude float,
  longitude float,
  name text,
  resource_id string,
  resource_type string
)
---
CREATE TABLE opportunities (
  country_code string,
  created_at timestamp,
  deadline timestamp,
  description text,
  division_code string,
  location_latitude float,
  location_longitude float,
  resource_id string,
  resource_type string,
  start timestamp,
  stop timestamp,
  updated_at timestamp,
  visibility string
)
---
CREATE TABLE peer_reviews (
  country_code string,
  created_at timestamp,
  deadline timestamp,
  description text,
  division_code string,
  location_latitude float,
  location_longitude float,
  resource_id string,
  resource_type string,
  start timestamp,
  stop timestamp,
  summary text,
  title text,
  updated_at timestamp,
  visibility string
)
---
CREATE TABLE posts (
  category string,
  content text,
  created_at timestamp,
  feed_resource_id string,
  feed_resource_type string,
  owner_id string,
  resource_id string,
  resource_type string,
  updated_at timestamp
)
---
CREATE TABLE programs (
  created_at timestamp,
  description text,
  resource_id string,
  resource_type string,
  space_id string,
  start timestamp,
  stop timestamp,
  summary text,
  title text,
  updated_at timestamp,
  visibility string
)
---
CREATE TABLE projects (
  created_at timestamp,
  description text,
  grant_information text,
  owner_id string,
  resource_id string,
  resource_type string,
  space_id string,
  summary text,
  title text,
  updated_at timestamp,
  visibility string
)
---
CREATE TABLE skills (
  created_at timestamp,
  name text,
  resource_id string,
  resource_type string,
  updated_at timestamp
)
---
CREATE TABLE spaces (
  created_at timestamp,
  category string,
  description text,
  owner_id string,
  resource_id string,
  resource_type string,
  summary text,
  title text,
  updated_at timestamp,
  visibility string
)
---
CREATE TABLE sustainable_development_goals (
  name text,
  resource_id string,
  resource_type string
)
---
CREATE TABLE tags (
  created_at timestamp,
  name text,
  resource_id string,
  resource_type string,
  updated_at timestamp
)
---
CREATE TABLE text_sections (
  category string,
  content text,
  created_at timestamp,
  resource_id string,
  resource_type string,
  title text,
  updated_at timestamp
)
---
CREATE TABLE translations (
  content text,
  created_at timestamp,
  language_code string,
  resource_id string,
  resource_type string,
  updated_at timestamp
)
---
CREATE TABLE users (
  birth_year bigint,
  created_at timestamp,
  description text,
  email text,
  gender string,
  first_name text,
  last_name text,
  location_id string,
  resource_id string,
  resource_type string,
  summary text,
  username text,
  updated_at timestamp
)
