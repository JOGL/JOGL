CREATE CLUSTER jogl
---
ALTER CLUSTER jogl ADD assets
---
ALTER CLUSTER jogl ADD challenges
---
ALTER CLUSTER jogl ADD comments
---
ALTER CLUSTER jogl ADD countries
---
ALTER CLUSTER jogl ADD divisions
---
ALTER CLUSTER jogl ADD events
---
ALTER CLUSTER jogl ADD languages
---
ALTER CLUSTER jogl ADD locations
---
ALTER CLUSTER jogl ADD opportunities
---
ALTER CLUSTER jogl ADD peer_reviews
---
ALTER CLUSTER jogl ADD posts
---
ALTER CLUSTER jogl ADD programs
---
ALTER CLUSTER jogl ADD projects
---
ALTER CLUSTER jogl ADD skills
---
ALTER CLUSTER jogl ADD spaces
---
ALTER CLUSTER jogl ADD sustainable_development_goals
---
ALTER CLUSTER jogl ADD tags
---
ALTER CLUSTER jogl ADD text_sections
---
ALTER CLUSTER jogl ADD translations
---
ALTER CLUSTER jogl ADD users
