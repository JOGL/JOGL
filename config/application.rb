# frozen_string_literal: true

require_relative 'boot'

require 'rails'

require 'action_cable/engine'
require 'action_controller/railtie'
require 'active_job/railtie'
require 'action_mailer/railtie'
require 'active_record/railtie'
require 'active_storage/engine'
require 'action_view/railtie'

require 'rack/cors'

Bundler.require(*Rails.groups)

module Jogl
  class Application < Rails::Application
    config.load_defaults 7.0

    config.api_only = true
    config.contact_email = ENV['CONTACT_EMAIL'] || 'hello@jogl.io'
    config.exceptions_app = self.routes
    config.graph_name = ENV['GRAPH_NAME'] || 'jogl'
    config.host = ENV['HOST'] || 'localhost:3000'
    config.rate_limit_options = { cooldown: 180, limit: 100, window: 60 }

    config.active_job.queue_adapter = :sucker_punch

    config.active_record.cache_versioning = true
    config.active_record.collection_cache_versioning = true
    
    config.active_storage.variant_processor = :mini_magick

    config.generators do |g|
      g.orm :active_record, primary_key_type: :uuid, foreign_key_type: :uuid
      g.test_framework :rspec
    end

    config.i18n.default_locale = :en

    config.logstasher.enabled = true
    config.logstasher.backtrace = false
    config.logstasher.logger_path = 'tmp/logstasher.log'
    
    config.paths.add 'app/lib', eager_load: true
    config.paths.add 'app/models/concerns', eager_load: true
    config.paths.add 'app/policies', eager_load: true
    config.paths.add 'app/serializers', eager_load: true
    config.paths.add 'app/validators', eager_load: true
  end
end
