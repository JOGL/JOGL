# frozen_string_literal: true

require 'active_support/core_ext/integer/time'

Rails.application.routes.default_url_options[:host] = Rails.application.config.host

Rails.application.configure do
  config.protocol = 'http'
  config.backend = "#{Rails.configuration.protocol}://#{Rails.configuration.host}:3000"
  config.frontend = "#{Rails.configuration.protocol}://#{Rails.configuration.host}:3001"

  config.cache_classes = false
  config.cache_store = :redis_cache_store, { host: ENV['REDIS_MASTER_SERVICE_HOST'], port: ENV['REDIS_MASTER_SERVICE_PORT'] }
  config.consider_all_requests_local = true
  config.eager_load = false
  config.hosts << Rails.configuration.host
      
  config.redis_options = { host: ENV['REDIS_MASTER_SERVICE_HOST'], port: ENV['REDIS_MASTER_SERVICE_PORT'] }

  config.search_cluster_name = nil

  config.action_controller.perform_caching = true
  config.action_controller.enable_fragment_cache_logging = true

  config.action_mailer.default_url_options = { host: ENV['HOST'] || 'localhost', port: 3000 }
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_caching = false
  config.action_mailer.smtp_settings = { address: ENV['EMAIL_HOST'] || 'localhost', port: 1025, openssl_verify_mode: 'none' }

  config.action_policy.cache_store = :memory_store

  config.active_record.migration_error = :page_load
  config.active_record.verbose_query_logs = true

  config.active_storage.service = :local

  config.active_support.deprecation = :log
  config.active_support.disallowed_deprecation = :raise
  config.active_support.disallowed_deprecation_warnings = []

  response = Faraday.get("http://#{ENV['LIBRETRANSLATE_SERVICE_HOST']}:#{ENV['LIBRETRANSLATE_SERVICE_PORT']}/languages")
  config.i18n.available_locales = JSON.parse(response.body).map { |language| language['code'].to_sym }

  config.public_file_server.headers = {
    'Cache-Control' => "public, max-age=#{2.days.to_i}"
  }

  config.after_initialize do
    Bullet.enable = true
    Bullet.rails_logger = true
  end
end
