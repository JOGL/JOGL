# frozen_string_literal: true

Rails.application.routes.draw do
  Rails.application.eager_load!

  mount Rswag::Api::Engine => '/docs'
  mount Rswag::Ui::Engine => '/docs'

  devise_for :users,
             controllers: {
               confirmations: 'auth/confirmations',
               registrations: 'auth/registrations',
               sessions: 'auth/sessions'
             },
             defaults: {
               format: :json
             },
             path: 'auth',
             path_names: {
               sign_in: 'signin',
               sign_out: 'signout',
               sign_up: 'signup'
             },
             skip: [:passwords]

  namespace :auth, constraints: { format: :json } do
    get  'passwords/token' => 'auth/passwords#token'
    post 'passwords/reset' => 'auth/passwords#reset', as: :reset_password
    post 'passwords/retrieve' => 'auth/passwords#retrieve', as: :retrieve_password
    post 'passwords/update' => 'auth/passwords#update', as: :update_password
  end

  resources :countries, param: :code, only: %i[index show] do
    resources :divisions, only: %i[index]
  end

  resources :languages, only: %i[index]

  post 'permissions' => 'permissions#index'

  resources :users, only: %i[] do
    resources :roles, only: %i[create index]

    delete 'roles' => 'roles#destroy'
  end
end
