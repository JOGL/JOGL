# frozen_string_literal: true

pin '@hotwired/stimulus', to: 'stimulus.js'
pin '@hotwired/stimulus-importmap-autoloader', to: 'stimulus-importmap-autoloader.js'
pin '@hotwired/turbo-rails', to: 'turbo.js'

pin 'alpinejs', to: 'https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js'
pin 'fontawesome', to: 'https://kit.fontawesome.com/a9917ff99c.js'

pin 'application'

pin_all_from 'app/javascript/controllers', under: 'controllers'
