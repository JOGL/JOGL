# frozen_string_literal: true

class GraphJob < ApplicationJob
  queue_as :default

  GRAPH = RedisGraph.new(
    Rails.configuration.graph_name,
    Rails.configuration.redis_options
  )

  def perform(method, *args)
    cmd = args.empty? ? method(method).call : method(method).call(*args)

    GRAPH.query(cmd) unless cmd.nil?
  end

  private

  def delete_node(node_class, id)
    %(MATCH (n:#{node_class} {id: #{wrap(id)}}) DETACH DELETE n)
  end

  def delete_edges(node_class, id)
    %(MATCH (n:#{node_class} {id: #{wrap(id)}})-[r]->() DELETE r)
  end

  def merge_edge(from_class, from_id, to_class, to_id)
    %(MATCH (from:#{from_class} {id: #{wrap(from_id)}}),
            (to:#{to_class} {id: #{wrap(to_id)}})
      MERGE (from)-[:x]->(to))
  end

  def merge_node(node_class, node)
    %(MERGE (n:#{node_class} #{unpack(node)}) RETURN n)
  end

  def update_node(node_class, node)
    id = node['id']
    update = node.except('id')

    return if update.empty?

    attributes = update.map { |key, value| "SET n.#{key} = #{wrap(value)}" }.join(' ')

    %(MATCH (n:#{node_class} {id: #{wrap(id)}}) #{attributes} RETURN n)
  end

  def unpack(node)
    attributes = node.map { |key, value| "#{key}: #{wrap(value)}" }.join(', ')
    "{#{attributes}}"
  end

  def wrap(attribute)
    %w[ActiveSupport::TimeWithZone Date DateTime String].include?(attribute.class.name) ? "'#{attribute}'" : attribute.to_s
  end
end
