# frozen_string_literal: true

require 'digest/sha2'

class DeviseMailer < Devise::Mailer
  default from: "admin@#{Rails.configuration.host}",
          message_id: "<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@#{Rails.configuration.host}>"
end
