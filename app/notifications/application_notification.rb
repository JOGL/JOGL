# frozen_string_literal: true

# ..
class ApplicationNotification < Noticed::Base
  def email_notifications?
    [:notifications, :email, kind.to_sym].reduce(recipient.options) do |value, key|
      break value if !value.nil? == value

      value[key] if value.is_a?(Hash) && value.key?(key)
    end
  end

  def kind
    self.class.name.underscore
  end
end
