# frozen_string_literal: true

class PhasePolicy < ApplicationPolicy
  cache :destroy?

  def destroy?
    allowed_to? :destroy?, record.project
  end

  def update?
    allowed_to? :update?, record.project
  end
end
