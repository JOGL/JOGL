# frozen_string_literal: true

# The base ActionPolicy for the application.
#
# There are two types of policies: object policies and object instance policies.
#
# Object policies check whether a user can do a thing with an object in general.
#
# Object instance policies check whether a user can do a thing with a particular object.
#
# You should always cache object instance policies like {#destroy?} and {#update?} in child classes.
#
# Don't cache object policies like {#create?}.
#
# @example Check whether or not a user can create a {Project}.
#   ProjectPolicy.new(actor: User.first).apply(:create?)
#
# @example Check whether or not a user can destroy a project.
#   ProjectPolicy.new(Project.first, actor: User.first).apply(:destroy?)
#
# @example Cache the results of every policy check for {#update?}.
#   class ProjectPolicy < ApplicationPolicy
#     cache :update?
#   end
class ApplicationPolicy < ActionPolicy::Base
  authorize :actor
  authorization_targets.delete(:user)

  # Construct a cache_key.
  #
  # Every time a user or object instance are updated, the cached policy becomes invalid.
  #
  # Also, this makes it easier to find cached permissions.
  #
  # @example Get a user's permissions directly from Rails.cache.
  #   Rails.cache.read("permission:#{user.id}:#{user.updated_at.to_i}:*")
  #
  # @example Or, use the method on the user.
  #   User.first.permissions
  def cache_key(_, record, _, rule)
    actor = "#{actor.class.name.downcase}:#{actor.id}:#{actor.updated_at.to_i}"
    record = "#{record.class.name.downcase}:#{record.id}:#{record.updated_at.to_i}"

    "permission:#{actor}:#{record}:#{rule}"
  end

  # Can a user create an instance of a model.
  def create?
    record.nil?
  end

  # Can a user delete a model instance and, possibly, its dependents.
  def destroy?
    owner?
  end

  # Can a user grant admin status to other users.
  def grant_admin?
    owner?
  end

  # Can a user grant member status to other users.
  def grant_member?
    admin?
  end

  # Can a user grant moderator status to other users.
  def grant_moderator?
    admin?
  end

  # Can a user list model records.
  def index?
    record.nil?
  end

  # Can a user interact with the model (join/follow/etc.).
  def interact?
    actor.nil? && (!record.respond_to?(:visibility) || record.open?)
  end

  # Can a user revoke admin status to other users.
  def revoke_admin?
    owner?
  end

  # Can a user revoke member status to other users.
  def revoke_member?
    admin?
  end

  # Can a user revoke moderator status to other users.
  def revoke_moderator?
    admin?
  end

  # Can a user view a model.
  def show?
    record.present? && (!record.respond_to?(:visibility) || record.visible? || moderator?)
  end

  # Can a user make changes to the properties of a model instance.
  def update?
    admin?
  end

  # Can a user change the owner of an object.
  def update_owner?
    owner?
  end

  protected

  def superadmin?
    actor.respond_to?(:has_role?) && actor.has_role?(:superadmin)
  end

  def self?
    (actor.instance_of?(record.class) && actor.id == record.id) || superadmin?
  end

  def owner?
    (record.respond_to?(:owner_id) && actor.id == record.owner_id) || self?
  end

  def admin?
    (actor.respond_to?(:has_role?) && actor.has_role?(:admin, record)) || owner?
  end

  def moderator?
    (actor.respond_to?(:has_role?) && actor.has_role?(:moderator, record)) || admin?
  end

  def member?
    (actor.respond_to?(:has_role?) && actor.has_role?(:member, record)) || moderator?
  end
end
