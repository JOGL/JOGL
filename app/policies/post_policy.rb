# frozen_string_literal: true

class PostPolicy < ActionPolicy::Base
  def destroy?
    owner? || allowed_to?(:update?, record.feed.resource)
  end
end
