# frozen_string_literal: true

# Person utilizing the application.
#
# @!attribute [rw] birth_year
#   @return [Integer]
# @!attribute [r] comments_count
#   @return [Integer] the number of Comments made by this User
# @!attribute [rw] description
#   @return [String]
# @!attribute [rw] email
#   @return [String]
# @!attribute [rw] first_name
#   @return [String]
# @!attribute [rw] gender
#   @return [String]
# @!attribute [r] groups_count
#   @return [Integer] the number of Groups in which this User resides
# @!attribute [rw] job_title
#   @return [String]
# @!attribute [rw] last_name
#   @return [String]
# @!attribute [rw] location
#   @return [Location]
# @!attribute [r] mentions_count
#   @return [Integer] the number of times this User has been Mentioned
# @!attribute [r] notifications_count
#   @return [Integer] the number of Notifications this User has received
# @!attribute [rw] options
#   @return [Hash]
# @!attribute [r] posts_count
#   @return [Integer] the number of Posts this User owns
# @!attribute [rw] profession
#   @return [String]
# @!attribute [r] projects_count
#   @return [Integer] the number of Projects this User owns
# @!attribute [r] spaces_count
#   @return [Integer] the number of Spaces this User owns
# @!attribute [rw] summary
#   @return [String]
# @!attribute [rw] username
#   @return [String]
class User < ApplicationRecord
  include Assetable
  include Deletable
  include Graphable
  include Feedable
  include Keyable
  include Imageable
  include Mediable
  include Optionable
  include Searchable
  include Skillable
  include Sluggable
  include Spammable
  include SustainableDevelopmentGoalable
  include Taggable
  include Translateable

  devise :async,
         :confirmable,
         :database_authenticatable,
         :lockable,
         :recoverable,
         :registerable,
         :rememberable,
         :trackable,
         :validatable

  imageable banner: nil, avatar: { thumb: { resize: '100x100' } }

  optionable(
    language: 'EN',
    notifications: {
      email: {
        new_comment_notification: true
      }
    }
  )

  searchable %w[description email first_name last_name summary username],
             %w[created_at location.id updated_at]

  sluggable method: :full_name

  spammable %w[description summary]

  translateable %w[description summary]

  ## FIELDS ##
  belongs_to :location, optional: true

  enum gender: { female: 0, male: 1, other: 2 }

  has_many :notifications, as: :recipient, dependent: :destroy
  has_many :posts, foreign_key: 'owner_id', inverse_of: :owner, dependent: :destroy

  has_many :roles, dependent: :destroy
  has_many :projects, foreign_key: 'owner_id', inverse_of: :owner, dependent: :destroy, through: :roles, source: :resource, source_type: 'Project'
  has_many :spaces, foreign_key: 'owner_id', inverse_of: :owner, dependent: :destroy, through: :roles, source: :resource, source_type: 'Space'

  has_many :users_groups, dependent: nil
  has_many :groups, through: :users_groups

  has_one :key, as: :resource, dependent: :destroy

  ## VALIDATIONS ##
  validates :birth_year, allow_nil: true, numericality: { integer_only: true,
                                                          greater_than: Time.zone.now.year - 125,
                                                          less_than: Time.zone.now.year - 15 }
  validates :first_name, :last_name, presence: true
  validates :first_name, :last_name, :username, length: { maximum: 32 }

  ## CALLBACKS ##
  before_validation -> { self.username = generate_username if username.blank? }

  ## SCOPES ##
  scope :with_role, ->(name) { joins(:roles).where(roles: { name: name }) }

  ## METHODS ##

  # (see #associated_objects)
  def associated_programs
    associated_objects(Program)
  end

  # (see #associated_objects)
  def associated_projects
    associated_objects(Project)
  end

  # (see #associated_objects)
  def associated_spaces
    associated_objects(Space)
  end

  # Get users with role of follower
  #
  # @return [Array<User>] the followers
  def followers
    User.join('LEFT JOIN roles ON roles.user_id = users.id')
        .where('roles.name = "follower" AND roles.resource_type = "User" AND roles.resource_id = :id', id: id)
  end

  # Get all users that user is following
  #
  # @return [Array<User>] the user's followed users
  def following
    User.join('LEFT JOIN roles ON roles.resource_id = users.id')
        .where('roles.name = "follower" AND roles.resource_type = "User" AND roles.user_id = :id', id: id)
  end

  # Combines first name and last name into a full name.
  #
  # @return [String]
  def full_name
    "#{first_name} #{last_name}"
  end

  # Creates a username with the first and last names.
  #
  # @return [String]
  def generate_username
    [first_name, last_name].join('.').gsub(/[^A-Za-z0-9.]+/, '').downcase
  end

  # Grants a {Role}
  #
  # @param name [Symbol]
  # @param resource [ApplicationRecord]
  def grant_role(name, resource = nil)
    Role.where(resource: resource, user_id: id).destroy_all

    if resource.present?
      Role.create!(name: name, resource: resource, user: self)
    else
      Role.create!(name: name, user: self)
    end
  end

  # Returns whether or not a {User} has a given {Role}
  #
  # @param name [Symbol]
  # @param resource [ApplicationRecord]
  def role?(name, resource = nil)
    Role.find_by(name: name, resource: resource, user_id: id).present?
  end

  # Revokes a {Role}
  #
  # @param name [Symbol]
  # @param resource [ApplicationRecord]
  def revoke_role(name, resource = nil)
    Role.find_by(name: name, resource: resource, user_id: id)&.destroy
  end

  private

  # Get all objects of a type related to the user by a role.
  #
  # @param type [ApplicationRecord] the type of objects to get
  # @return [ActiveRecord::Relation]
  def associated_objects(type)
    type.joins("LEFT JOIN roles ON roles.resource_id = #{type.name.downcase.pluralize}.id")
        .where('roles.resource_type = :type AND roles.user_id = :id', type: type.name, id: id)
  end
end
