# frozen_string_literal: true

# ..
#
# @!attributes [rw] description
#   @return [String]
# @!attributes [rw] location
#   @return [Location]
# @!attributes [r] moments_count
#   @return [Integer] number of Moments in this Project
# @!attributes [r] owner
#   @return [User]
# @!attributes [r] phases_count
#   @return [Integer] number of Phases in this Project
# @!attributes [r] space
#   @return [Space]
# @!attributes [rw] start
#   @return [Date]
# @!attributes [rw] stop
#   @return [Date]
# @!attributes [rw] summary
#   @return [String]
# @!attributes [rw] title
#   @return [String]
class Project < ApplicationRecord
  include Assetable
  include Deletable
  include Feedable
  include Graphable
  include Imageable
  include Mediable
  include Searchable
  include Sluggable
  include Spammable
  include SustainableDevelopmentGoalable
  include Taggable
  include TextSectionable
  include Translateable
  include Visibility

  graphable %w[visibility]

  imageable banner: nil

  searchable %w[description summary title],
             %w[created_at
                owner_id
                space_id
                updated_at
                visibility]

  sluggable field: :title

  spammable %w[description summary title]

  translateable %w[description summary title]

  ## FIELDS ##
  belongs_to :location, optional: true
  belongs_to :owner, class_name: 'User'
  belongs_to :space, optional: true

  counter_culture :owner
  counter_culture :space

  has_many :accepted_challenges_projects, -> { accepted }, class_name: 'ChallengesProject', inverse_of: :project, dependent: :destroy
  has_many :challenges, through: :accepted_challenges_projects, class_name: 'Challenge', source: :challenge

  has_many :pending_challenges_projects, -> { pending }, class_name: 'ChallengesProject', inverse_of: :project, dependent: :destroy
  has_many :pending_challenges, through: :pending_challenges_projects, class_name: 'Challenge', source: :challenge

  has_many :phases, dependent: :destroy
  has_many :roles, as: :resource, dependent: :destroy
  has_many :users, through: :roles, source: :user

  ## VALIDATIONS ##
  validates :description, :title, presence: true
  validates :description, length: { maximum: 1024 }
  validates :summary, length: { maximum: 512 }
  validates :title, length: { maximum: 128 }

  ## CALLBACKS ##
  after_initialize do |project|
    project.text_sections << TextSection.new(category: :about, title: 'Grant Information')
  end

  ## METHODS ##

  # Get posts from project feed
  #
  # @return [Array<Feed>] the posts
  def posts
    Feed.find_by(resource_id: id).posts
  end

  # Get project users with role of member
  #
  # @return [Array<User>] the members
  def members
    User.with_role(:member, self)
  end
end
