# frozen_string_literal: true

# A searchable keyword or sequence of characters.
#
# @!attribute [r] name
#   @return [String] name
class Tag < ApplicationRecord
  include Graphable
  include Searchable

  searchable %w[name],
             %w[created_at updated_at]

  ## FIELDS ##
  has_many :tags_resources, dependent: :destroy
  has_many :activities, through: :tags_resources, source: :resource, source_type: 'Activity'
  has_many :programs, through: :tags_resources, source: :resource, source_type: 'Program'
  has_many :projects, through: :tags_resources, source: :resource, source_type: 'Project'
  has_many :spaces, through: :tags_resources, source: :resource, source_type: 'Space'
  has_many :users, through: :tags_resources, source: :resource, source_type: 'User'

  ## VALIDATIONS ##
  validates :name, presence: true, length: { maximum: 64 }, format: { with: /\A[A-Za-z0-9]+\z/ }

  ## METHODS ##
  def format
    self.name = name.gsub(/[^A-Za-z0-9]/i, '')
  end
end
