# frozen_string_literal: true

# ..
class SkillsResource < ApplicationRecord
  ## FIELDS ##
  belongs_to :resource, polymorphic: true
  belongs_to :skill

  ## VALIDATIONS ##
end
