# frozen_string_literal: true

# Link to a resource outside of the application.
#
# !@attribute [r] resource
#   @return [ApplicationRecord] model to which this one belongs
# !@attribute [rw] text
#   @return [String] name of entity being linked to
# !@attribute [rw] url
#   @return [String] URL of entity being linked to
class ExternalLink < ApplicationRecord
  include Imageable
  include Translateable

  imageable icon: { resize: '64x64' }

  translateable %w[text]

  ## FIELDS ##
  belongs_to :resource, polymorphic: true

  ## VALIDATIONS ##
  validates :text, :url, presence: true, length: { maximum: 128 }
  validates :url, url: true

  ## CALLBACKS ##
  before_validation -> { self.text = url if text.blank? }

  ## METHODS ##
  def self.youtube_embed_link(id)
    ExternalLink.create(url: "https://www.youtube.com/embed/#{id}")
  end

  def self.youtube_link(id)
    ExternalLink.create(url: "https://www.youtube.com/watch?v=#{id}")
  end
end
