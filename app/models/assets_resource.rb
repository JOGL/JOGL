# frozen_string_literal: true

# ..
class AssetsResource < ApplicationRecord
  ## FIELDS ##
  belongs_to :asset
  belongs_to :resource, polymorphic: true

  ## VALIDATIONS ##
end
