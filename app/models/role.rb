# frozen_string_literal: true

# Defined relation between a User and the application (in general) or a resource (in particular.)
#
# @!attribute [r] name
#   @return [String]
class Role < ApplicationRecord
  ALLOWED_NAMES = %w[admin clapper follower member moderator superadmin].freeze
  ALLOWED_RESOURCES = %w[Program Project Space User].freeze

  ## FIELDS ##
  belongs_to :user
  belongs_to :resource, polymorphic: true, optional: true

  ## VALIDATIONS ##
  validates :name, presence: true, length: { maximum: 32 }, inclusion: { in: ALLOWED_NAMES }
  validates :resource_type, inclusion: { in: ALLOWED_RESOURCES }, allow_nil: true

  ## SCOPE ##
  default_scope { order(resource_type: :asc, resource_id: :asc) }
end
