# frozen_string_literal: true

module Optionable
  extend ActiveSupport::Concern

  included do
    ## FIELDS ##
    serialize :options, JsonbSerializer

    ## CALLBACKS ##
    after_initialize do |obj|
      obj.options = obj.class.default_options if options.blank? && obj.class.default_options
    end
  end

  class_methods do
    attr_reader :default_options

    private

    # Initialize the concern.
    #
    # @param default_options [Hash] default options for the model.
    def optionable(default_options = {})
      @default_options = default_options
    end
  end
end
