# frozen_string_literal: true

module Searchable
  extend ActiveSupport::Concern

  included do
    after_create :index

    after_update :reindex

    after_destroy :deindex

    def index
      SearchJob.perform_now('index', self.class.name, id, self.class.search_fields, self.class.search_attributes)
    end

    def deindex
      SearchJob.perform_now('deindex', self.class.name, search_id)
    end

    def reindex
      SearchJob.perform_now('reindex', self.class.name, id, self.class.search_fields, self.class.search_attributes)
    end
  end

  class_methods do
    attr_reader :search_attributes
    attr_reader :search_fields

    def search(match)
      Search.new(match).within(self)
    end

    def index_drop
      Search.drop(self)
    end

    def index_truncate
      Search.truncate(self)
    end

    private

    def searchable(search_fields, search_attributes = [])
      @search_fields = search_fields
      @search_attributes = search_attributes
    end
  end
end
