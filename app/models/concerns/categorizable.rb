# frozen_string_literal: true

module Categorizable
  extend ActiveSupport::Concern

  included do
    validates :category, presence: true
  end

  class_methods do
    private

    # Initialize the concern.
    #
    # @param default_options [Hash] default options for the model.
    def categorizable(categories = {})
      enum category: categories
    end
  end
end
