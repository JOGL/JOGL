# frozen_string_literal: true

module Feedable
  extend ActiveSupport::Concern

  included do
    has_one :feed, as: :resource, dependent: :destroy

    after_create lambda {
      Feed.create!(
        options: self.class.feed_options.nil? ? Feed.default_options : self.class.feed_options,
        resource: self
      )
    }
  end

  class_methods do
    attr_reader :feed_options

    private

    def feedable(options = {})
      @feed_options = options
    end
  end
end
