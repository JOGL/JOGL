# frozen_string_literal: true

module Skillable
  extend ActiveSupport::Concern

  included do
    has_many :skills_resources, as: :resource, dependent: :destroy
    has_many :skills, through: :skills_resources
  end
end
