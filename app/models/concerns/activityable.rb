# frozen_string_literal: true

# Add multiple Activities to an object.
module Activityable
  extend ActiveSupport::Concern

  included do
    has_many :activities, as: :resource, dependent: :destroy

    delegate :challenges, :events, :opportunities, :peer_reviews, to: :activities
  end

  def past_activities
    activities.where('stop <= ?', Time.zone.now)
  end

  def upcoming_activities
    activities.where('stop >= ?', Time.zone.now)
  end
end
