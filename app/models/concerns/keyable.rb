# frozen_string_literal: true

module Keyable
  extend ActiveSupport::Concern

  included do
    has_one :key, as: :resource, dependent: :destroy

    after_create -> { Key.create(resource: self) }
  end
end
