# frozen_string_literal: true

module Visibility
  extend ActiveSupport::Concern

  included do
    ## FIELDS ##
    enum visibility: { hidden: 0,
                       findable: 1,
                       open: 2,
                       archived: 3 }

    ## SCOPES ##
    scope :archived, -> { where(visibility: :archived) }
    scope :findable, -> { where(visibility: :findable) }
    scope :hidden, -> { where(visibility: :hidden) }
    scope :open, -> { where(visibility: :open) }

    scope :visible, -> { where('visibility > 0') }

    def visible?
      visibility.to_i.positive?
    end
  end
end
