# frozen_string_literal: true

module Fundable
  extend ActiveSupport::Concern

  included do
    has_one :fund, as: :resource, dependent: :destroy
  end
end
