# frozen_string_literal: true

# Set of 17 interlinked global goals designed by the United Nations. [Sustainable development](https://sdgs.un.org/).
#
# @!attribute [r] color
#   @return [String]
# @!attribute [r] name
#   @return [String]
# @!attribute [r] number
#   @return [Integer]
class SustainableDevelopmentGoal < ApplicationRecord
  include Graphable
  include Searchable
  include Translateable

  searchable %w[name]

  translateable %w[name]

  ## FIELDS ##
  has_many :sustainable_development_goals_resources, dependent: :destroy
  has_many :activities, through: :sustainable_development_goals_resources, source: :resource, source_type: 'Activity'
  has_many :programs, through: :sustainable_development_goals_resources, source: :resource, source_type: 'Program'
  has_many :projects, through: :sustainable_development_goals_resources, source: :resource, source_type: 'Project'
  has_many :spaces, through: :sustainable_development_goals_resources, source: :resource, source_type: 'Space'
  has_many :users, through: :sustainable_development_goals_resources, source: :resource, source_type: 'User'

  ## VALIDATIONS ##
  validates :color, :name, :number, presence: true
  validates :color, length: { maximum: 6 }
  validates :name, length: { maximum: 64 }
end
