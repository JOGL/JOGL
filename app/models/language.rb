# frozen_string_literal: true

# ISO 639-2 A system of signals for communicating.
#
# !@attribute [r] code
#   @return [String] ISO 639-2 code
# !@attribute [r] name
#   @return [String] ISO 639-2 name
class Language < ApplicationRecord
  include Searchable

  searchable %w[code name]

  self.cache_versioning = true

  ## FIELDS ##
  self.primary_key = :code

  has_many :translations, inverse_of: :language, foreign_key: :language_code, dependent: :destroy

  ## VALIDATIONS ##
  validates :code, :name, presence: true

  validates :code, length: { maximum: 2 }
  validates :name, length: { maximum: 96 }

  ## SCOPES ##
  scope :supported, -> { where(supported: true) }

  ## METHODS ##
  def to_sym
    code.downcase.to_sym
  end
end
