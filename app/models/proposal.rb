# frozen_string_literal: true

# Application of a Project to a PeerReview.
class Proposal < ApplicationRecord
  include Assetable
  include Groupable
  include Mediable
  include SustainableDevelopmentGoalable
  include Taggable
  include TextSectionable

  ## FIELDS ##
  belongs_to :peer_review
  belongs_to :project

  ## CALLBACKS ##
  after_initialize do |application|
    application.groups << Group.new(name: 'Reviewers')
    application.groups << Group.new(name: 'Team')

    application.text_sections << TextSection.new(category: :about, title: 'Funding')
  end
end
