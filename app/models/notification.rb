# frozen_string_literal: true

# Message intended for a recipient.
#
# !@attribute [r] params
#   @return [Hash] information about the notification contents
# !@attribute [r] read_at
#   @return [DateTime]
# !@attribute [rw] recipient
#   @return [ApplicationRecord]
# !@attribute [rw] type
#   @return [String] category of notification, e.g., NewCommentNotification
class Notification < ApplicationRecord
  include Noticed::Model

  ## FIELDS ##
  belongs_to :recipient, polymorphic: true

  counter_culture :recipient, column_name: proc { |model| model.recipient_type == 'User' ? 'notifications_count' : nil }

  ## VALIDATIONS ##
  validates :type, presence: true
end
