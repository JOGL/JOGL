# frozen_string_literal: true

# Collection of Posts attached to a resource.
#
# !@attribute [rw] options
#   @return [Hash]
# !@attribute [r] posts_count
#   @return [Integer] the number of posts in this feed
class Feed < ApplicationRecord
  include Deletable
  include Optionable

  optionable allow_posting_to_all: true

  ## FIELDS ##
  belongs_to :resource, polymorphic: true

  has_many :posts, dependent: :destroy

  ## VALIDATIONS ##
  validates :options, presence: true
end
