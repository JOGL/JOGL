# frozen_string_literal: true

# Reference to a resource within a Post.
#
# !@attribute [r] post
#   @return [Post]
# !@attribute [r] resource
#   @return [ApplicationRecord]
class Mention < ApplicationRecord
  include Graphable

  ## FIELDS ##
  belongs_to :post
  belongs_to :resource, polymorphic: true

  counter_culture :resource, column_name: proc { |model| model.resource_type == 'User' ? 'mentions_count' : nil }

  ## VALIDATIONS ##
  validates :post_id, uniqueness: { scope: %i[resource_id resource_type], case_insensitive: true }
end
