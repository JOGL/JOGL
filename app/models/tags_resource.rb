# frozen_string_literal: true

# ..
class TagsResource < ApplicationRecord
  ## FIELDS ##
  belongs_to :resource, polymorphic: true
  belongs_to :tag

  ## VALIDATIONS ##
end
