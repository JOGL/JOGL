# frozen_string_literal: true

# Serialize JSONB.
class JsonbSerializer
  def self.load(data)
    return if data.nil?

    ActiveJob::Arguments.send(:deserialize_argument, data)
  end

  def self.dump(data)
    return if data.nil?

    ActiveJob::Arguments.send(:serialize_argument, deep_clean(data))
  end

  def self.deep_clean(data)
    case data
    when Array
      data.map { |datum| deep_clean(datum) }
    when Hash
      data.deep_transform_values { |value| deep_clean(value) }
    when Regexp
      data.to_s
    else
      data
    end
  end
end
