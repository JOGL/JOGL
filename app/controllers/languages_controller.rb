# frozen_string_literal: true

class LanguagesController < ApplicationController
  def index
    response = Rails.cache.fetch("#{controller_name}:#{action_name}", expires_in: 1.day) do
      LanguageBlueprint.render(Language.all.order(code: :asc))
    end

    render json: response
  end
end
