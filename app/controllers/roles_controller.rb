# frozen_string_literal: true

class RolesController < ApplicationController
  before_action :authenticate_actor!
  before_action :load_resource, only: %i[create destroy]
  before_action :load_policy, only: %i[create destroy]
  before_action :load_user

  def create
    permission = "grant_#{params.require(:name)}?".to_sym

    head :unauthorized and return unless @policy.apply(permission)

    @user.grant_role(params.require(:name), @resource)

    head :created
  end

  def destroy
    permission = "revoke_#{params.require(:name)}?".to_sym

    head :unauthorized and return unless @policy.apply(permission)

    @user.revoke_role(params.require(:name), @resource)

    head :ok
  end

  def index
    render json: @user.roles
  end

  private

  def load_policy
    klass = ApplicationPolicy.descendants.find do |policy|
      policy.name == "#{@resource.class.name.camelize}Policy"
    end

    @policy = klass.new(@resource, actor: @current_actor)
  end

  def load_resource
    @resource = ApplicationRecord.descendants.find do |klass|
      klass.name == params.require(:resource_type).camelize
    end.find(params.require(:resource_id))
  end

  def load_user
    @user = User.find(params.require(:user_id))
  end
end
