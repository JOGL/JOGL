# frozen_string_literal: true

module Auth
  class RegistrationsController < Devise::RegistrationsController
    def create
      user = User.new(sign_up_params)

      if user.save
        render json: user.key.to_json
      else
        render json: { errors: user.errors.full_messages }, status: :unprocessable_entity
      end
    end
  end
end
