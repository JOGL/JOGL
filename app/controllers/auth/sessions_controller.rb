# frozen_string_literal: true

module Auth
  class SessionsController < Devise::SessionsController
    wrap_parameters :user, format: [:json]

    def create
      user = User.find_by(email: sign_in_params[:email])

      render plain: user.key.id, status: :ok and return if user&.valid_password?(sign_in_params[:password])

      render json: { errors: { 'email or password' => ['is invalid'] } }, status: :unauthorized
    end
  end
end
