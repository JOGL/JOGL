# frozen_string_literal: true

# ..
class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  # Omniauth callback for [Eventbrite](http://eventbrite.com).
  # Adds an Eventbrite Service to the Serviceable `serviceable_type`, `serviceable_id` with the owner of `user_id`.
  def eventbrite
    auth = request.env['omniauth.auth']
    token = auth['credentials']['token']

    params = request.env['omniauth.params']

    redirect_link = params.key?('redirect_link') ? params['redirect_link'] : ''
    resource_id = params['resource_id']
    resource_type = params['resource_type']&.camelize
    user_id = params['user_id']

    render status: :not_found and return unless Serviceable.serviceable?(resource_type, resource_id, user_id)

    service = { category: :eventbrite,
                owner_id: user_id,
                serviceable_id: resource_id,
                serviceable_type: resource_type,
                token: token }

    Service.upsert(service, unique_by: :unique_serviceable_index)

    redirect_to redirect_link, notice: "New service created for #{resource_type}."
  end

  def failure
    redirect_to root_path
  end
end
