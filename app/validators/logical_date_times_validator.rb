# frozen_string_literal: true

class LogicalDateTimesValidator < ActiveModel::Validator
  def validate(record)
    stop_after_start record
    deadline_after_start record
    deadline_before_stop record
  end

  private

  def stop_after_start(record)
    return if !record.respond_to?(:start) || !record.respond_to?(:stop)

    return if record.start.nil? || record.stop.nil?

    return if record.stop >= record.start

    record.errors.add(:stop, "#{record.stop} must be after start time: #{record.start}")
  end

  def deadline_after_start(record)
    return if !record.respond_to?(:deadline) || !record.respond_to?(:start)

    return if record.deadline.nil? || record.start.nil?

    return if record.deadline >= record.start

    record.errors.add(:deadline, "#{record.deadline} must be after start: #{record.start}")
  end

  def deadline_before_stop(record)
    return if !record.respond_to?(:deadline) || !record.respond_to?(:stop)

    return if record.deadline.nil? || record.start.nil?

    return if record.deadline <= record.stop

    record.errors.add(:deadline, "#{record.deadline} must be before stop: #{record.stop}")
  end
end
