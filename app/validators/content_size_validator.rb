# frozen_string_literal: true

class ContentSizeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return unless value.attached?

    return if value.byte_size < content_size

    value.purge
    record.errors.add(attribute, :content_size, options)
  end

  def to_sym
    :content_size_validator
  end

  private

  def content_size
    options.fetch(:with)
  end
end
