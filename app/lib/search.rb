# frozen_string_literal: true

# Interface for searching via Manticore.
#
# Uses the builder pattern and chaining so that queries can be built step-wise.
# The order of steps does not matter, but match must be set in the initializer.
# This is because it makes no sense to have a query without a match clause.
#
# @example Chain method calls together to build and then execute a query
#   Search.new('foo bar').within(Program, Project, Space).order('created_at ASC').limit(50).execute
class Search
  ## CONSTANTS ##
  CLIENT = Mysql2::Client.new(host: ENV['MANTICORE_MASTER_SERVICE_HOST'],
                              port: ENV['MANTICORE_MASTER_SERVICE_PORT_MYSQL'])

  attr_accessor :match

  ## METHODS ##

  # Create a new Search query instance.
  # You can set match later to re-use the same query object.
  #
  # @example Re-use a query for a different match.
  #   search = Search.new('foo bar').within(Space).limit(10)
  #   results = search.execute
  #   search.match = 'bar baz'
  #   results = search.execute
  #
  # @param [String] the text for which to search
  # @return [Search]
  def initialize(match)
    @indexes = ApplicationRecord.descendants
                                .keep_if { |model| model.include?(Searchable) }
                                .map { |model| self.class.klass_to_index(model) }
    @match = match.tr("'", '')
  end

  # Execute the prepared query and return an array of results.
  #
  # @example Search all Searchable models for 'foo bar'
  #   Search.new('foo bar').execute
  #
  # @return [Array<ApplicationRecord>]
  def execute
    prepare

    CLIENT.query(@query).map do |row|
      row['resource_type'].constantize.find(row['resource_id'])
    end
  end

  # Set the attributes of a query.
  #
  # @example Set the attributes
  #   Search.new('foo bar').attributes(category: { visibility: 'open' }, created_at: 1.day.ago...Time.now + 1.day)
  #
  # @param [Hash]
  # @return [Search]
  def attributes(attributes)
    @attributes = attributes

    self
  end

  # Set the limit (count, offset) of returned results.
  #
  # @example Set the limit without an offset
  #   Search.new('foo bar').limit(100)
  # @example Set the limit with an offset
  #   Search.new('foo bar').limit(100, 200)
  #
  # @param [Integer] the number of records to return
  # @param [Integer] the offset to begin returning records from
  # @return [Search]
  def limit(count, offset = 0)
    @limit = [count, offset]

    self
  end

  # Set the order of returned results.
  #
  # @example Set the order to get the latest results first
  #   Search.new('foo bar').order('created_at DESC')
  #
  # @param [String] the order statement
  # @return [Search]
  def order(order)
    @order = order

    self
  end

  # Generate the query that will be executed.
  #
  # @return [String]
  def query
    prepare

    @query
  end

  # Limit the search range to within the given models.
  #
  # @example Only search within programs, projects, and spaces
  #   Search.new('foo bar').within(Program, Project, Space)
  #
  # @param [ApplicationRecord]
  # @return [Search]
  def within(*klasses)
    @indexes = klasses.map { |klass| self.class.klass_to_index(klass) }

    self
  end

  # Remove specific models from the search range.
  #
  # @example Remove text sections and translations from the results
  #   Search.new('foo bar').without(TextSection, Translation)
  #
  # @param [ApplicationRecord]
  # @return [Search]
  def without(*klasses)
    indexes = klasses.map { |klass| self.class.klass_to_index(klass) }

    @indexes = @indexes.reject { |index| indexes.include?(index) }

    self
  end

  ## CLASS METHODS ##

  # Convert the model to an index name.
  # This is particularly necessary because index names change if Manticore is being run in cluster mode.
  #
  # @param [ApplicationRecord]
  # @return [String] the name of the index
  def self.klass_to_index(klass)
    index = klass.name.underscore.downcase.pluralize

    return "#{Rails.configuration.search_cluster_name}:#{index}" unless Rails.configuration.search_cluster_name.nil?

    index
  end

  # Drop the index from Manticore.
  #
  # @example Drop an index
  #   Search.drop(Project)
  # @param [ApplicationRecord]
  def self.drop(klass)
    query = %(DROP TABLE IF EXISTS #{klass_to_index(klass)})

    CLIENT.query(query)
  end

  # Truncate the index in Manticore.
  #
  # @example Truncate an index
  #   Search.truncate(Space)
  #
  # @param [ApplicationRecord]
  def self.truncate(klass)
    query = %(TRUNCATE TABLE #{klass_to_index(klass)})

    CLIENT.query(query)
  end

  private

  def attributes_clause
    lines = @attributes.each.map do |key, value|
      case value
      when String
        "#{key} = '#{value}'"
      when Integer, Float
        "#{key} = #{value}"
      when Array
        "#{key} IN #{value}".tr('[', '(').tr(']', ')').tr('\"', "'")
      when Range
        "#{key} BETWEEN #{value.begin} and #{value.end}"
      end
    end.join('AND ')

    "\nAND #{lines}"
  end

  def limit_clause
    "\nLIMIT #{@limit.second}, #{@limit.first}"
  end

  def order_clause
    "\nORDER BY #{@order}"
  end

  def prepare
    @query = %(SELECT resource_id, resource_type
               FROM #{@indexes.join(', ')}
               WHERE MATCH('#{@match}'))

    @query += attributes_clause if @attributes
    @query += order_clause if @order
    @query += limit_clause if @limit

    @query = @query.gsub(/[\n\s]+/, ' ')
  end
end
