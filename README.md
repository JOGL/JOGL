# JOGL

Gitlab
[![pipeline status](https://gitlab.com/JOGL/JOGL/badges/develop/pipeline.svg)](https://gitlab.com/JOGL/JOGL/-/commits/develop)
[![coverage report](https://gitlab.com/JOGL/JOGL/badges/develop/coverage.svg)](https://gitlab.com/JOGL/JOGL/-/commits/develop)

JOGL is a 100% open-source application built with [Ruby on Rails](https://rubyonrails.org/).

We fully welcome contributions to the code!
Feel free to browse through the [issues](https://gitlab.com/JOGL/jogl/-/issues).

For more detailed information, please read our guide to [contributing](https://gitlab.com/JOGL/JOGL/-/blob/main/CONTRIBUTING.md).

## DEVELOP

JOGL relies on various [environment variables](https://github.com/bkeepers/dotenv/) in order to operate correctly.
We suggest creating a `.env` file with same variables as our [.env.example](.env.example).

Run the application in development mode:

```sh
%> ./bin/dev
```

### RUBY

We are currently using Ruby [3.0.x](https://www.ruby-lang.org/en/downloads/) and suggest using a tool like [rbenv](https://github.com/rbenv/rbenv#installation) to manage Ruby versions.

### SPAM DETECTION

You will need to have [CRM114](https://en.wikipedia.org/wiki/CRM114_(program)) installed in order to detect spam.

### EXTERNAL SERVICES

The easiest way to get JOGL's supporting services running is with [Docker Compose](https://docs.docker.com/compose/install/).

```
%> docker compose -f docker-compose-services.yml up -d
```

Create the [manticore search](https://manticoresearch.com/) indexes:

```
%> bundle exec rake search:sql[migrate]
```

## DOCUMENT

We use [yard](https://github.com/lsegal/yard/) for documenting our codebase.

```sh
%> bundle exec yardoc -m markdown -M redcarpet app/**/*.rb
```

Check out our [documentation](https://jogl.gitlab.io/JOGL/).

## TEST

We use [RSpec](https://rspec.info/).

```sh
%> bundle exec rspec
```

Check out our [current test coverage](https://jogl.gitlab.io/JOGL/coverage).

## AUDIT

Check used gems for any known security vulnerabilities:

```sh
%> bundle exec bundler-audit
```

Check application code for security vulnerabilities:

```sh
%> bundle exec brakeman
```

Check out our [security report](https://jogl.gitlab.io/JOGL/analysis/security.html).

## LINT

We use [RuboCop](https://github.com/rubocop/rubocop) for checking code quality and formatting.
See [.rubocop.yml](.rubocop.yml) for our configuration.

```sh
%> bundle exec rubocop
```

You can format a single file (or directory):

```sh
%> bundle exec rubocop --fix-layout file.rb 
```

Check out our [linting results](https://jogl.gitlab.io/JOGL/analysis/quality.html).
