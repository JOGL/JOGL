# frozen_string_literal: true

require 'swagger_helper'

require Rails.root.join('spec/support/helpers.rb')

RSpec.describe 'divisions' do
  path '/countries/{country_code}/divisions' do
    get 'index divisions' do
      tags 'divisions'

      consumes 'application/json'
      produces 'application/json'

      parameter name: 'country_code', in: :path, type: :string, description: 'country code'

      response '200', 'success', type: :string do
        schema type: :array,
               items: {
                 '$ref' => '#/components/schemas/division'
               }

        let(:country_code) { 'fr' }

        run_test!

        after do |example|
          example.metadata[:response][:examples] =
            { 'application/json' => JSON.parse(response.body, symbolize_names: true) }
        end
      end
    end
  end
end
