# frozen_string_literal: true

require 'swagger_helper'

require Rails.root.join('spec/support/helpers.rb')

RSpec.describe 'sessions' do
  path '/auth/signin' do
    post 'signin' do
      tags 'auth'

      consumes 'application/json'
      produces 'text/plain'

      parameter name: :credentials, in: :body, schema: {
        type: :object,
        properties: {
          email: { type: :string },
          password: { type: :string }
        },
        required: %i[email password]
      }

      response '200', 'success', type: :string do
        schema type: :string

        let(:credentials) do
          { email: create(:user).email, password: 'password' }
        end

        run_test!
      end
    end
  end
end
