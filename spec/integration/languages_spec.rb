# frozen_string_literal: true

require 'swagger_helper'

require Rails.root.join('spec/support/helpers.rb')

RSpec.describe 'languages' do
  path '/languages' do
    get 'index languages' do
      tags 'languages'

      consumes 'application/json'
      produces 'application/json'

      response '200', 'success', type: :string do
        schema type: :array,
               items: {
                 '$ref' => '#/components/schemas/language'
               }

        run_test!

        after do |example|
          example.metadata[:response][:examples] =
            { 'application/json' => JSON.parse(response.body, symbolize_names: true) }
        end
      end
    end
  end
end
