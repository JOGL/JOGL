# frozen_string_literal: true

require 'swagger_helper'

require Rails.root.join('spec/support/helpers.rb')

RSpec.describe 'countries' do
  path '/countries' do
    get 'index countries' do
      tags 'countries'

      consumes 'application/json'
      produces 'application/json'

      response '200', 'success' do
        schema type: :array,
               items: {
                 '$ref' => '#/components/schemas/country'
               }

        let(:'X-API-KEY') { create(:user, confirmed_at: Time.zone.now).key.id }

        run_test!

        after do |example|
          example.metadata[:response][:examples] =
            { 'application/json' => JSON.parse(response.body, symbolize_names: true) }
        end
      end
    end
  end

  path '/countries/{code}' do
    get 'show country' do
      tags 'countries'

      consumes 'application/json'
      produces 'application/json'

      parameter name: 'code', in: :path, type: :string, description: 'code'

      response '200', 'success' do
        schema '$ref' => '#/components/schemas/country'

        let(:code) { 'FR' }

        run_test!
      end
    end
  end
end
