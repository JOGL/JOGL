# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'fundable' do
  it { is_expected.to have_one(:fund) }
end
