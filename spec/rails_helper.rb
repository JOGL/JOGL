# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../config/environment', __dir__)

abort('The Rails environment is running in production mode!') if Rails.env.production?

require 'factory_bot_rails'
require 'rake'
require 'rspec/rails'
require 'spec_helper'
require 'validate_url/rspec_matcher'

Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

Rails.application.load_tasks

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

RSpec.configure do |config|
  config.filter_rails_from_backtrace!
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.infer_spec_type_from_file_location!
  config.use_transactional_fixtures = true

  DatabaseCleaner.allow_remote_database_url = true

  Geocoder.configure(lookup: :test)
  Geocoder::Lookup::Test.add_stub(
    '8 Rue Charles V, Paris, Paris, France, 75004',
    [{ coordinates: [48.85289, 2.36292] }]
  )

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
    DatabaseCleaner.strategy = :transaction

    Rake::Task['graph:clear'].invoke
    Rake::Task['search:reset'].invoke

    Country.create!(code: 'FR', name: 'France')
    Division.create!(code: 'FR-75', country_code: 'FR', name: 'Paris')

    Language.create!(code: 'EN', name: 'English', supported: true)
    Language.create!(code: 'FR', name: 'French', supported: true)
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end
