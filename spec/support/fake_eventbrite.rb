# frozen_string_literal: true

require 'sinatra/base'
require_relative 'stub_api'

class FakeEventbrite < StubApi
  get '/v3/users/me/organizations/' do
    json_response 200, 'eventbrite/organizations.json'
  end

  get '/v3/organizations/:id/events/' do
    json_response 200, 'eventbrite/events.json'
  end
end
