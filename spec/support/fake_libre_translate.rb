# frozen_string_literal: true

require 'sinatra/base'
require_relative 'stub_api'

class FakeLibreTranslate < StubApi
  get '/languages' do
    json_response 200, 'libre_translate/languages.json'
  end

  post '/detect' do
    json_response 200, 'libre_translate/detect.json'
  end

  post '/translate' do
    json_response 200, 'libre_translate/translate.json'
  end
end
