# frozen_string_literal: true

# == Schema Information
#
# Table name: assets
#
#  id         :uuid             not null, primary key
#  name       :string(64)       not null
#  search_id  :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :asset do
    name { FFaker::Book.title[0..63] }
  end
end
