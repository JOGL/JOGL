# frozen_string_literal: true

# == Schema Information
#
# Table name: locations
#
#  id         :uuid             not null, primary key
#  address_id :uuid
#  latitude   :float
#  longitude  :float
#  name       :string(64)
#  search_id  :bigint
#
FactoryBot.define do
  factory :location do
    address { build(:address) }
    latitude { FFaker::Geolocation.lat }
    longitude { FFaker::Geolocation.lng }
    name { FFaker::Venue.name[0..63] }
  end
end
