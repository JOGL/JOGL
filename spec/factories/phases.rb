# frozen_string_literal: true

# == Schema Information
#
# Table name: phases
#
#  id         :uuid             not null, primary key
#  project_id :uuid             not null
#  color      :string(6)        default("000000"), not null
#  deleted    :boolean          default(FALSE), not null
#  start      :date             not null
#  stop       :date             not null
#  title      :string(128)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :phase do
    project { build(:project) }
    start { Time.zone.now }
    stop { 1.day.from_now }
    title { FFaker::Book.title[0..63] }
  end
end
