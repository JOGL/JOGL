# frozen_string_literal: true

# == Schema Information
#
# Table name: text_sections
#
#  id            :uuid             not null, primary key
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  category      :integer          default("about"), not null
#  content       :string(1024)
#  deleted       :boolean          default(FALSE), not null
#  position      :integer          default(0), not null
#  search_id     :bigint
#  title         :string(128)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
FactoryBot.define do
  factory :text_section do
    category { TextSection.categories.keys[rand(TextSection.categories.keys.count - 1)] }
    content { FFaker::Book.description[0..511] }
    resource { build(:space) }
    title { FFaker::Book.title[0..63] }
  end
end
