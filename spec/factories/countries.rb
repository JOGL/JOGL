# frozen_string_literal: true

# == Schema Information
#
# Table name: countries
#
#  code            :string(2)        not null, primary key
#  name            :string(64)       not null
#  search_id       :bigint
#  divisions_count :integer          default(0), not null
#
FactoryBot.define do
  factory :country do
    code { 'FR' }
    name { 'France' }
  end
end
