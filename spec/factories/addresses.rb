# frozen_string_literal: true

# == Schema Information
#
# Table name: addresses
#
#  id            :uuid             not null, primary key
#  country_code  :string(2)        not null
#  division_code :string(6)        not null
#  city_locality :string(32)
#  postal_code   :string(16)
#  street        :string(64)
#
FactoryBot.define do
  factory :address do
    city_locality { 'Paris' }
    division { Division.find_by(country_code: 'FR') }
    country { Country.find('FR') }
    postal_code { '75004' }
    street { '8 Rue Charles V' }
  end
end
