# frozen_string_literal: true

# == Schema Information
#
# Table name: activities
#
#  id            :uuid             not null, primary key
#  activity_id   :uuid
#  location_id   :uuid
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  deadline      :datetime
#  description   :string(1024)
#  kind          :string(32)       not null
#  options       :jsonb            not null
#  search_id     :bigint
#  slug          :string(64)       not null
#  start         :datetime
#  stop          :datetime
#  summary       :string(512)
#  title         :string(128)      not null
#  visibility    :integer          default("hidden"), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
FactoryBot.define do
  factory :event do
    kind { 'Event' }
    description { FFaker::Book.description[0..255] }
    location { build(:location) }
    resource { build(:challenge) }
    start { 1.day.ago }
    stop { 1.day.from_now }
    title { FFaker::Book.title }
  end
end
