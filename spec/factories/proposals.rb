# frozen_string_literal: true

# == Schema Information
#
# Table name: proposals
#
#  id             :uuid             not null, primary key
#  peer_review_id :uuid             not null
#  project_id     :uuid             not null
#  score          :integer
#  title          :string(128)      not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
FactoryBot.define do
  factory :proposal do
    peer_review { build(:peer_review) }
    project { build(:project) }
    title { FFaker::Book.title[0..127] }
  end
end
