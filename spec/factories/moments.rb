# frozen_string_literal: true

# == Schema Information
#
# Table name: moments
#
#  id          :uuid             not null, primary key
#  phase_id    :uuid             not null
#  date        :date             not null
#  description :string(1024)
#  title       :string(128)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
FactoryBot.define do
  factory :moment do
    date { Time.zone.now }
    phase { build(:phase) }
    title { FFaker::Book.title[0..63] }
  end
end
