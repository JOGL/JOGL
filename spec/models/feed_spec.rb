# frozen_string_literal: true

# == Schema Information
#
# Table name: feeds
#
#  id            :uuid             not null, primary key
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  deleted       :boolean          default(FALSE), not null
#  options       :jsonb            not null
#  posts_count   :integer          default(0), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/deletable.rb')
require Rails.root.join('spec/models/concerns/optionable.rb')

RSpec.describe Feed, type: :model do
  subject { create(:space).feed }

  it_behaves_like 'deletable'
  it_behaves_like 'optionable'

  it { is_expected.to belong_to(:resource) }

  it { is_expected.to have_many(:posts) }
end
