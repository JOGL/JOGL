# frozen_string_literal: true

# == Schema Information
#
# Table name: media
#
#  id            :uuid             not null, primary key
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  deleted       :boolean          default(FALSE), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/deletable.rb')

RSpec.describe Media, type: :model do
  subject { create(:space).media }

  it_behaves_like 'deletable'

  it { is_expected.to belong_to(:resource) }
  it { is_expected.to have_many(:external_links) }
  it { is_expected.to have_many_attached(:documents) }
  it { is_expected.to have_many_attached(:images) }
end
