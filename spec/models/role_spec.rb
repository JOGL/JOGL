# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Role, type: :model do
  subject { create(:role) }

  it { is_expected.to belong_to(:resource).optional }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_length_of(:name) }
  it { is_expected.to validate_inclusion_of(:name).in_array(Role::ALLOWED_NAMES) }
  it { is_expected.to validate_inclusion_of(:resource_type).in_array(Role::ALLOWED_RESOURCES).allow_nil }
end
