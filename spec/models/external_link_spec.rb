# frozen_string_literal: true

# == Schema Information
#
# Table name: external_links
#
#  id            :uuid             not null, primary key
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  text          :string(128)      not null
#  url           :string(128)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/translateable.rb')

RSpec.describe ExternalLink, type: :model do
  subject { create(:external_link) }

  it_behaves_like 'translateable'

  it { is_expected.to belong_to(:resource) }

  it { is_expected.to validate_presence_of(:url) }
  it { is_expected.to validate_length_of(:text) }
  it { is_expected.to validate_length_of(:url) }
  it { is_expected.to validate_url_of(:url) }

  it 'defaults to using the url as text' do
    external_link = create(:external_link, text: nil)

    expect(external_link.text).to eq external_link.url
  end

  it 'returns a valid youtube embed link' do
    expect(described_class.youtube_embed_link('dQw4w9WgXcQ')).to be_an(described_class)
  end

  it 'returns a valid youtube link' do
    expect(described_class.youtube_link('dQw4w9WgXcQ')).to be_an(described_class)
  end
end
