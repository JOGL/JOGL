# frozen_string_literal: true

# == Schema Information
#
# Table name: groups
#
#  id            :uuid             not null, primary key
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  name          :string(64)
#  users_count   :integer          default(0), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
require 'rails_helper'

RSpec.describe Group, type: :model do
  subject { create(:group) }

  it { is_expected.to belong_to(:resource) }

  it { is_expected.to have_many(:users) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_length_of(:name) }
end
