# frozen_string_literal: true

# == Schema Information
#
# Table name: comments
#
#  id         :uuid             not null, primary key
#  owner_id   :uuid             not null
#  post_id    :uuid             not null
#  content    :string(1024)     not null
#  deleted    :boolean          default(FALSE), not null
#  search_id  :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/searchable.rb')
require Rails.root.join('spec/models/concerns/spammable.rb')
require Rails.root.join('spec/models/concerns/translateable.rb')

RSpec.describe Comment, type: :model do
  subject { create(:comment) }

  it_behaves_like 'searchable'
  it_behaves_like 'spammable'
  it_behaves_like 'translateable'

  it { is_expected.to belong_to(:owner) }
  it { is_expected.to belong_to(:post) }

  it { is_expected.to validate_presence_of(:content) }
  it { is_expected.to validate_length_of(:content) }

  it 'sends the post owner a notification after save' do
    notifications = Notification.where(recipient: subject.post.owner)

    expect(notifications.count).to eq 1
  end
end
