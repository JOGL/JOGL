# frozen_string_literal: true

# == Schema Information
#
# Table name: assets
#
#  id         :uuid             not null, primary key
#  name       :string(64)       not null
#  search_id  :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/searchable.rb')
require Rails.root.join('spec/models/concerns/sluggable.rb')
require Rails.root.join('spec/models/concerns/translateable.rb')

RSpec.describe Asset, type: :model do
  subject { create(:asset) }

  it_behaves_like 'searchable'
  it_behaves_like 'translateable'

  it { is_expected.to have_many(:assets_resources) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_length_of(:name) }
end
