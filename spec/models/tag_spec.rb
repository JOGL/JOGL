# frozen_string_literal: true

# == Schema Information
#
# Table name: tags
#
#  id         :uuid             not null, primary key
#  name       :string(64)       not null
#  search_id  :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/searchable.rb')

RSpec.describe Tag, type: :model do
  subject { create(:tag) }

  it_behaves_like 'searchable'

  it { is_expected.to have_many(:activities) }
  it { is_expected.to have_many(:programs) }
  it { is_expected.to have_many(:projects) }
  it { is_expected.to have_many(:spaces) }
  it { is_expected.to have_many(:users) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_length_of(:name) }

  it 'formats the name correctly' do
    subject.name = 'I @m not correct!'

    subject.format

    expect(subject.save).to be true
  end
end
