# frozen_string_literal: true

# == Schema Information
#
# Table name: locations
#
#  id         :uuid             not null, primary key
#  address_id :uuid
#  latitude   :float
#  longitude  :float
#  name       :string(64)
#  search_id  :bigint
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/searchable.rb')

RSpec.describe Location, type: :model do
  subject { create(:location) }

  it_behaves_like 'searchable'

  it { is_expected.to belong_to(:address).optional }

  it { is_expected.to validate_length_of :name }

  describe 'geocoder' do
    it 'writes the latitude and longitude' do
      address = Address.create!(street: '8 Rue Charles V',
                                city_locality: 'Paris',
                                country: Country.find('FR'),
                                division: Division.find('FR-75'),
                                postal_code: '75004')

      location = described_class.create!(name: 'Centre de Recherches Interdisciplinaires',
                                         address: address)

      expect(location.coordinates).not_to be_nil
    end
  end
end
