# frozen_string_literal: true

# == Schema Information
#
# Table name: applications
#
#  id             :uuid             not null, primary key
#  peer_review_id :uuid             not null
#  project_id     :uuid             not null
#  score          :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
require Rails.root.join('spec/models/concerns/assetable.rb')
require Rails.root.join('spec/models/concerns/groupable.rb')
require Rails.root.join('spec/models/concerns/mediable.rb')
require Rails.root.join('spec/models/concerns/sustainable_development_goalable.rb')
require Rails.root.join('spec/models/concerns/taggable.rb')
require Rails.root.join('spec/models/concerns/text_sectionable.rb')

RSpec.describe Proposal, type: :model do
  subject { build(:proposal) }

  it_behaves_like 'assetable'
  it_behaves_like 'groupable'
  it_behaves_like 'mediable'
  it_behaves_like 'sustainable_development_goalable'
  it_behaves_like 'taggable'
  it_behaves_like 'text_sectionable'

  it { is_expected.to belong_to(:peer_review) }
  it { is_expected.to belong_to(:project) }
end
