# frozen_string_literal: true

# == Schema Information
#
# Table name: mentions
#
#  id            :uuid             not null, primary key
#  post_id       :uuid             not null
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  deleted       :boolean          default(FALSE), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
require 'rails_helper'

RSpec.describe Mention, type: :model do
  subject { create(:mention) }

  it { is_expected.to belong_to(:post) }
  it { is_expected.to belong_to(:resource) }

  it { is_expected.to validate_uniqueness_of(:post_id).scoped_to(%i[resource_id resource_type]).case_insensitive }
end
