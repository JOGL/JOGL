# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'sluggable' do
  it { is_expected.to validate_length_of(:slug) }
  it { is_expected.to validate_uniqueness_of(:slug) }

  it 'sets a default slug' do
    expect(subject.slug).not_to be_nil
  end
end
