# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'categorizable' do
  it { is_expected.to define_enum_for(:category) }

  it { is_expected.to validate_presence_of(:category) }
end
