# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'deletable' do
  it 'limits queries to only those rows where deleted = false' do
    expect(described_class.all.to_sql).to eq described_class.all.except(:where).where(deleted: false).to_sql
  end
end
