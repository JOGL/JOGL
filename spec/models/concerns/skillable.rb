# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'skillable' do
  include ActiveJob::TestHelper

  it { is_expected.to have_many(:skills) }
end
