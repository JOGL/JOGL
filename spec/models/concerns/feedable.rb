# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'feedable' do
  include ActiveJob::TestHelper

  it { is_expected.to have_one(:feed) }

  it 'creates a feed after_create' do
    expect(subject.feed).not_to be_nil
  end

  it 'uses default (feed) options' do
    expect(subject.feed.options).not_to be_empty
  end
end
