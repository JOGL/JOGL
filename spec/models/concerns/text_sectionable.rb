# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'text_sectionable' do
  include ActiveJob::TestHelper

  it { is_expected.to have_many(:text_sections) }

  it 'returns a grouped hash of text sections' do
    create_list(:text_section, 5, resource: subject)

    expect(subject.grouped_text_sections).to be_a(Hash)
  end
end
