# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'assetable' do
  include ActiveJob::TestHelper

  it { is_expected.to have_many(:assets_resources) }
end
