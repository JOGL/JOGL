# frozen_string_literal: true

# == Schema Information
#
# Table name: sustainable_development_goals
#
#  id        :uuid             not null, primary key
#  color     :string(6)        default("000000"), not null
#  name      :string(64)       not null
#  number    :integer          not null
#  search_id :bigint
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/searchable.rb')
require Rails.root.join('spec/models/concerns/translateable.rb')

RSpec.describe SustainableDevelopmentGoal, type: :model do
  subject { create(:sustainable_development_goal) }

  it_behaves_like 'searchable'
  it_behaves_like 'translateable'

  it { is_expected.to have_many(:activities) }
  it { is_expected.to have_many(:projects) }
  it { is_expected.to have_many(:spaces) }
  it { is_expected.to have_many(:users) }

  it { is_expected.to validate_length_of(:color) }
  it { is_expected.to validate_length_of(:name) }

  it { is_expected.to validate_presence_of(:color) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:number) }
end
