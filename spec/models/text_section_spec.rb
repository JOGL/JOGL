# frozen_string_literal: true

# == Schema Information
#
# Table name: text_sections
#
#  id            :uuid             not null, primary key
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  category      :integer          default("about"), not null
#  content       :string(1024)
#  deleted       :boolean          default(FALSE), not null
#  position      :integer          default(0), not null
#  search_id     :bigint
#  title         :string(128)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/categorizable.rb')
require Rails.root.join('spec/models/concerns/deletable.rb')
require Rails.root.join('spec/models/concerns/searchable.rb')
require Rails.root.join('spec/models/concerns/translateable.rb')

RSpec.describe TextSection, type: :model do
  subject { create(:text_section) }

  it_behaves_like 'categorizable'
  it_behaves_like 'deletable'
  it_behaves_like 'searchable'
  it_behaves_like 'translateable'

  it { is_expected.to belong_to(:resource) }

  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_length_of(:content) }
  it { is_expected.to validate_length_of(:title) }
end
