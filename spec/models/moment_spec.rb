# frozen_string_literal: true

# == Schema Information
#
# Table name: moments
#
#  id          :uuid             not null, primary key
#  phase_id    :uuid             not null
#  date        :date             not null
#  description :string(1024)
#  title       :string(128)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/translateable.rb')

RSpec.describe Moment, type: :model do
  subject { create(:moment) }

  it_behaves_like 'translateable'

  it { is_expected.to belong_to(:phase) }

  it { is_expected.to validate_presence_of(:date) }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_length_of(:description) }
  it { is_expected.to validate_length_of(:title) }
end
